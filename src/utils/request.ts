import router from '@/router'
import { useUserStore } from '@/stores'
import axios from 'axios'
import type { Method } from 'axios'
import { showToast } from 'vant'
import type { User } from '@/types/user'

// 设置基地址
export const baseURL = 'https://consult-api.itheima.net/'
// 响应超时时间
const timeout = 3000

const instance = axios.create({
  baseURL,
  timeout
})

// 请求拦截器
instance.interceptors.request.use(
  (config) => {
    // 修改config信息
    // 获取token
    const userStore = useUserStore()
    if (userStore.user?.token && config.headers) {
      config.headers.Authorization = `Bearer ${userStore.user?.token}`
    }

    return config
  },
  (err) => {
    Promise.reject(err)
  }
)

// 响应拦截器
instance.interceptors.response.use(
  (res) => {
    // 后台约定，响应成功，但是code不是10000，是业务逻辑失败
    if (res.data.code !== 10000) {
      // 提示用户错误信息
      showToast(res.data?.message || '业务失败')
      return Promise.reject(res.data)
    }
    // 过滤无效数据
    return res.data
  },
  (err) => {
    // 如果状态码为401则清除用户信息，并且跳转到登录页面；登录成功返回原页面
    if (err.response.status === 401) {
      // 清空用户信息
      const userStore = useUserStore()
      userStore.clearUser()
      //跳转到登录页面并携带当前页面地址
      router.push({
        path: '/login',
        query: { returnUrl: router.currentRoute.value.fullPath }
      })
    }
    return Promise.reject(err)
  }
)

//定义后台返回的数据类型
type Data<T> = {
  code: number
  message: string
  data: T
}

// 封装请求工具函数
export const request = <T>(
  url: string,
  method: Method = 'GET',
  submitData?: object
) => {
  return instance.request<T, Data<T>>({
    url,
    method,
    [method.toUpperCase() === 'GET' ? 'params' : 'data']: submitData
  })
}
