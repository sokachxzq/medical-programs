import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import pinia from './stores'
import 'virtual:svg-icons-register'

// 引入vant全局样式
import 'vant/lib/index.css'
import '@/styles/main.scss'
//路由进度条样式
import 'nprogress/nprogress.css'

const app = createApp(App)

app.use(pinia)
app.use(router)
app.mount('#app')
