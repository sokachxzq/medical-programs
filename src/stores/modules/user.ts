import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { User } from '@/types/user'

// 用户仓库
const useUserStore = defineStore(
  'user',
  () => {
    //1.用户信息
    const user = ref<User>()

    //2.修改用户信息
    const setUser = (u: User) => {
      user.value = u
    }

    //3.清空用户信息
    const clearUser = () => {
      user.value = undefined
    }

    return { user, setUser, clearUser }
  },
  {
    persist: true
  }
)

export default useUserStore
