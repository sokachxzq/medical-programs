// 维护pinia
import { createPinia } from 'pinia'
import persist from 'pinia-plugin-persistedstate'
import useUserStore from './modules/user'

const pinia = createPinia()

// 使用pinia插件做数据持久化
pinia.use(persist)

export default pinia
export { useUserStore }
