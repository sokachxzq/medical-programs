import CpNavbar from '@/components/cp-navbar.vue'
import CpIcons from '@/components/cp-icons.vue'
import CpradioBtn from '@/components/cp-radioBtn.vue'

declare module 'vue' {
  interface GlobalComponents {
    CpNavbar: typeof CpNavbar
    CpIcons: typeof CpIcons
    CpradioBtn: typeof CpradioBtn
  }
}
